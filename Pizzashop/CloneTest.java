package Pizzashop;

import Pizzashop.food.Pizza;

public class CloneTest {
	public static void main(String[] args) {
		Pizza a = new Pizza(1);
		Pizza b = (Pizza) a.clone();
		System.out.println(a==b);
		System.out.println(a.equals(b));
		System.out.println(a.getPrice()==b.getPrice());
		System.out.println(a.getSize()==b.getSize());
		System.out.println(a.getToppings());
		System.out.println(b.getToppings());
	}
}
