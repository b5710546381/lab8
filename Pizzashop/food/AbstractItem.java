package Pizzashop.food;

public abstract class AbstractItem implements OrderItem {
	public static final String [] sizes = { "None", "Small", "Medium", "Large" };
	private int size;
	public AbstractItem(int size) {
		this.size = size;
	}
	public abstract double getPrice();
	public Object clone() {
		Object clone = null;
		try {
			clone = super.clone();
		}catch (CloneNotSupportedException c1) {
			System.out.println("Item.clone: " + c1.getMessage() );
		}
		return clone;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int newSize) {
		size = newSize;
	}
}
